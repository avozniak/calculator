require 'colorize'
require_relative 'bmi'
require_relative 'calculator'

loop do
  session = Validations::Calculator.new
  case session.init_choise
    when 1
    session.greeting
      case session.choise
        when 1..4
        session.first_operand
        session.second_operand
        session.calculate
        puts "Result of #{ session.operation } is #{ session.result }".black.on_white if session.result
        else
        puts 'No such option'
      end
    when 2
      bmi = Bmi.new
      bmi.validate
      bmi.calculate
      bmi.recomendation
    else
      puts 'Make your choise, young Jedi'
  end
  break if session.init_choise == 0
end