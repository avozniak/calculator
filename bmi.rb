require_relative 'calculator'

class Bmi
  include Validations

  attr_reader :result

  def initialize
    puts 'Enter your weight (kg)'
    @weight = validate_number(gets.chomp).abs

    puts 'Enter your height (cm)'
    @height = validate_number(gets.chomp).abs * 0.01
  end

  def calculate
    @result = (@weight / (@height**2)).round(2)
  end

  def validate
    if @weight > 200 || @weight < 40
      puts 'Something wrong with your weight, so the BMI results will be inadequate'.red.on_black
    elsif @height > 250 || @height < 120
      puts 'Something wrong with your height, so the BMI results will be inadequate'.red.on_black
    end
  end

  def recomendation
    case @result
      when 0..18.5
        puts " Your BMI is #{ @result }, Underweight ".green.on_black
      when 18.5..24.9
        puts " Your BMI is #{ @result }, Normal weight ".green.on_black
      when 24.9..29.9
        puts " Your BMI is #{ @result }, Overweight ".green.on_black
      else
        puts "Your BMI is #{ @result }, Obesity ".green.on_black
    end
  end
end