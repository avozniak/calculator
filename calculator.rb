module Validations

  def validate_number number
    until number =~ /\d/
      puts 'Not a number! Enter a number!'
      number = gets.chomp
    end
    number.to_i
  end

  class Calculator

    include Validations

    attr_reader :init_choise, :choise, :operation, :result

    def initialize
      puts 'What would you like to do?'
      puts '[1] Calculate numbers'
      puts '[2] Calculate body mass index'
      puts '[0] Take me out of here immediately'

      @init_choise = validate_number(gets.chomp)
    end

    def greeting
      puts 'Lets calculate numbers. What would you like to do?'
      puts '[1] Addition'
      puts '[2] Subtraction'
      puts '[3] Multiply'
      puts '[4] Division'

      @choise = gets.to_i
    end

    def first_operand
      puts 'Enter first operand'
      @first_operand = validate_number(gets.chomp)
    end

    def second_operand
      puts 'Enter second operand'
      @second_operand = validate_number(gets.chomp)
    end

    def calculate
      @result = addition if @choise == 1
      @result = subtraction if @choise == 2
      @result = multiply if @choise == 3
      @result = division if @choise == 4
    end

    def addition
      @operation = 'Addition'
      @first_operand + @second_operand
    end

    def subtraction
      @operation = 'Subtraction'
      @first_operand - @second_operand
    end

    def multiply
      @operation = 'Multiply'
      @first_operand * @second_operand
    end

    def division
      @operation = 'Division'
      if @second_operand == 0
        puts 'Can not divide by zero!'.red.on_black
      else
        (@first_operand / @second_operand.to_f).round(2)
      end
    end
  end
end
